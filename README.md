# Web Application



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/e.gopinath151/web-application.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/e.gopinath151/web-application/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## problem statement 1
When it comes to scheduling periodic tasks like downloading a list of ISINs every 24 hours, one reliable and commonly used solution is utilizing a task scheduler. There are several options available, and the choice often depends on the specific requirements and the technology stack of the application. Some popular task schedulers include:

Cron Jobs: Cron is a time-based job scheduler in Unix-like operating systems. It allows you to schedule commands or scripts to run at specific intervals or times.

Celery: Celery is a distributed task queue system in Python that allows you to run asynchronous tasks, including periodic ones.

AWS CloudWatch Events: If you are using Amazon Web Services (AWS), CloudWatch Events can be used to trigger tasks at specified intervals.

Azure Logic Apps: If you are using Microsoft Azure, Logic Apps provides workflow automation and can be used for periodic tasks.

Google Cloud Scheduler: If you are using Google Cloud Platform, Google Cloud Scheduler allows you to trigger tasks at regular intervals.

Choosing the right task scheduler depends on factors like your existing infrastructure, programming language, scalability requirements, and cost considerations.

Regarding reliability and scalability, many of the mentioned task schedulers are known to be reliable when properly configured and maintained. However, the scale depends on the number of tasks and the complexity of the tasks being scheduled. Some challenges that might arise with task scheduling at scale include:

Overlapping Tasks: If tasks take longer to complete than the scheduled interval, there can be overlapping tasks, leading to resource contention and inefficiencies.

Failure Handling: Handling task failures, retries, and error reporting is crucial for maintaining a reliable system.

Load Balancing: As the number of tasks increases, load balancing becomes important to distribute the workload evenly.

Monitoring and Logging: Proper monitoring and logging are essential for identifying and resolving issues promptly.

To address these challenges at scale in production, you can consider the following:

Distributed Systems: Utilize distributed systems and load balancers to manage the workload efficiently.

Task Queue Prioritization: Prioritize tasks based on their criticality to ensure that important tasks are completed promptly.

Error Handling and Retries: Implement robust error handling and retry mechanisms for tasks that might fail occasionally.

Monitoring and Alerting: Set up comprehensive monitoring and alerting systems to detect issues proactively.

Horizontal Scaling: Scale the task scheduler horizontally by adding more resources or nodes to meet increased demand.

Containerization and Orchestration: Use containerization platforms like Docker and container orchestration tools like Kubernetes for managing task execution in a scalable and resilient manner.

Remember that task scheduling is just one part of a larger system, and it's essential to consider the overall architecture and design when building a reliable and scalable production system.

## problem statement 2
Use Flask when:

Lightweight and Simplicity: Flask is a micro-framework and is designed to be lightweight and minimalistic. It gives you more flexibility and control over your application's structure. If you prefer a simple, straightforward framework with fewer built-in components, Flask might be a better choice.

RESTful APIs: If you primarily need to build RESTful APIs or small web applications with minimal server-side rendering, Flask's simplicity makes it an excellent option.

Microservices: Flask's micro-framework nature makes it suitable for building microservices and integrating them into larger systems.

Learning and Prototyping: If you are new to web development or want to quickly prototype an idea, Flask's smaller learning curve and ease of use can be advantageous.

Use Django when:

Full-Featured Web Applications: Django is a full-featured web framework with many built-in components and batteries included. It provides an extensive set of tools for building complex web applications quickly.

Rapid Development: Django's "batteries-included" approach allows you to get a project up and running faster, as it provides functionalities like an ORM, authentication, admin interface, and more out of the box.

Complex Database Queries: If your application involves complex database queries and relationships, Django's powerful ORM (Object-Relational Mapping) can simplify database interactions and management.

Security and Authentication: Django has robust built-in security features and provides authentication mechanisms, making it a good choice for applications that require user registration and login.

Admin Interface: Django's automatic admin interface is a great time-saver for managing application data during development and can be used as a quick back-office interface.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
